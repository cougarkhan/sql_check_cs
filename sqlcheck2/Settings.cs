﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace sqlcheck2
{
    class Settings
    {
        public RootObject server { get; set; }

        public Settings(string path_to_config_file)
        {
            try
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                string json = File.ReadAllText(path_to_config_file);
                List<RootObject> configs = ser.Deserialize<List<RootObject>>(json);
                server = configs[0];
            }
            catch (Exception e)
            {
            }
        }
    }

    public class Oracle
    {
        public string protocol { get; set; }
        public string host { get; set; }
        public string port { get; set; }
        public string service_name { get; set; }
        public string sql_query { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class Mssql
    {
        public string instance { get; set; }
        public string database { get; set; }
        public string sql_query { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class Email
    {
        public string sender { get; set; }
        public List<string> recipients { get; set; }
        public string smtpserver { get; set; }
    }

    public class RootObject
    {
        public Oracle oracle { get; set; }
        public Mssql mssql { get; set; }
        public Email email { get; set; }
    }
}
