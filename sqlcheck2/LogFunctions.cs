﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Diagnostics; 

namespace sqlcheck2
{
    class LogFunctions
    {
        private string f_dt;
        private string f_lt;

        public string format_datetime
        {
            get
            {
                return f_dt = (DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            set
            {
                f_dt = value;
            }
        }

        public string format_logtime
        {
            get
            {
                return f_lt = (DateTime.Now.ToString("HH:mm:ss"));
            }
            set
            {
                f_lt = value;
            }
        }

    }

    class Timer_functions
    {
        private Stopwatch sw;
        public TimeSpan ts;

        public Timer_functions()
        {
            sw = new Stopwatch();
        }

        public void set()
        {
            sw.Restart();
        }

        public string get()
        {
            sw.Stop();
            string elapsed_time;
            ts = sw.Elapsed;

            elapsed_time = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            return elapsed_time;
        }
    }
}
