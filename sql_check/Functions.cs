﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace sql_check
{
    class Functions
    {
        private const bool DEBUG = true;

        public static ReturnObject checkOracle(RootObject server)
        {
            ReturnObject return_object = new ReturnObject();            

            string conn_string = "user id=" + server.oracle.username + 
                                 ";password=" + server.oracle.password + 
                                 "; data source=(DESCRIPTION=(ADDRESS=(PROTOCOL=" + server.oracle.protocol + 
                                 ")(HOST=" + server.oracle.host + 
                                 ")(PORT=" + server.oracle.port + 
                                 "))(CONNECT_DATA=(SERVICE_NAME=" + server.oracle.service_name + ")))";
            OracleConnection conn = new OracleConnection(conn_string);
            OracleCommand cmd = conn.CreateCommand();

            int attempts = 1;

            if (conn.State == ConnectionState.Broken)
            {
                conn.Close();
            }

            while (conn.State != ConnectionState.Open && attempts <= 3)
            {
                try
                {
                    return_object.messages.Add(String.Format("Attempt {0}.", attempts));
                    attempts++;
                    conn.Open();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                }
                catch (Exception e)
                {
                    if (DEBUG)
                    {
                        return_object.messages.Add(e.ToString());
                    }
                }
            }

            if (conn.State == ConnectionState.Open)
            {
                cmd.CommandText = server.oracle.sql_query;
                OracleDataReader reader = cmd.ExecuteReader();

                List<string> rows = new List<string>();
                while (reader.Read())
                {
                    rows.Add(reader[0].ToString());
                }

                if (rows.Count > 0)
                {
                    return_object.result = true;
                    return_object.messages.Add("Successfully tested database query.");
                    conn.Dispose();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return return_object;
                }
                else
                {
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return_object.result = false;
                    return_object.messages.Add(String.Format("Reader Object had {0} rows.", rows.Count));
                    conn.Dispose();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return return_object;
                }
            }
            else
            {
                return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                return_object.result = false;
                return_object.messages.Add("Connection could not be opened.");
                return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                conn.Dispose();
                return return_object;
            }
        }

        public static ReturnObject checkMSSQL(RootObject server)
        {
            ReturnObject return_object = new ReturnObject();

            string conn_string = "server=" + server.mssql.instance +
                                ";Trusted_Connection=yes;" +
                                "database=" + server.mssql.database +
                                ";connection timeout=5";
            //comment again another onefdsfd

            SqlConnection conn = new SqlConnection(conn_string);
            SqlCommand cmd = conn.CreateCommand();

            int attempts = 1;

            if (conn.State == ConnectionState.Broken)
            {
                conn.Close();
            }

            while (conn.State != ConnectionState.Open && attempts <= 3)
            {
                try
                {
                    return_object.messages.Add(String.Format("Attempt {0}.", attempts));
                    attempts++;
                    conn.Open();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                }
                catch (Exception e)
                {
                    if (DEBUG)
                    {
                        return_object.messages.Add(e.ToString());
                    }
                }
            }

            if (conn.State == ConnectionState.Open)
            {
                cmd.CommandText = server.mssql.sql_query;
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> rows = new List<string>();
                while (reader.Read())
                {
                    rows.Add(reader[0].ToString());
                }

                if (rows.Count > 0)
                {
                    return_object.result = true;
                    return_object.messages.Add("Successfully tested database query.");
                    conn.Dispose();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return return_object;
                }
                else
                {
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return_object.result = false;
                    return_object.messages.Add(String.Format("Reader Object had {0} rows.", rows.Count));
                    conn.Dispose();
                    return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                    return return_object;
                }
            }
            else
            {
                return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                return_object.result = false;
                return_object.messages.Add("Connection could not be opened.");
                return_object.messages.Add(String.Format("Connection state is {0}.", conn.State));
                conn.Dispose();
                return return_object;
            }
        }
    }

    class ReturnObject
    {
        public bool result { get; set; }
        public List<string> messages { get; set; }

        public ReturnObject()
        {
            messages = new List<string>();
        }
    }

    class SendMail
    {
        public SendMail()
        {
        }

        public void sendLogEmail(Settings config, int engine, ReturnObject result)
        {
            string hostname = Environment.GetEnvironmentVariable("computername");
            MailMessage message = new MailMessage();
            SmtpClient smtp_client = new SmtpClient(config.server.email.smtpserver);

            if (result.result == false)
            {
                switch (engine)
                {
                    case 1:
                        foreach (string recipient in config.server.email.recipients)
                        {
                            message.To.Add(recipient);
                        }
                        message.Subject = String.Format("SQL Connection from {0} to {2} FAILED.", hostname, config.server.oracle.host);
                        message.From = new MailAddress(config.server.email.sender);
                        message.Body = string.Join(Environment.NewLine,result.messages);
                        smtp_client.Send(message);
                        break;
                    case 2:
                        foreach (string recipient in config.server.email.recipients)
                        {
                            message.To.Add(recipient);
                        }
                        message.Subject = String.Format("SQL Connection from {0} to {2} FAILED.", hostname, config.server.mssql.instance);
                        message.From = new MailAddress(config.server.email.sender);
                        message.Body = string.Join(Environment.NewLine,result.messages);
                        smtp_client.Send(message);
                        break;
                    default:
                        foreach (string recipient in config.server.email.recipients)
                        {
                            message.To.Add(recipient);
                        }
                        message.Subject = String.Format("SQL Connection on {0} FAILED.", hostname);
                        message.From = new MailAddress(config.server.email.sender);
                        message.Body = string.Join(Environment.NewLine,result.messages);
                        smtp_client.Send(message);
                        break;
                }
            }

            
        }
    }
}
