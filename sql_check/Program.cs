﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Oracle.DataAccess.Client;
using System.IO;

namespace sql_check
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ORACLE_ENGINE = "1";
            const string MSSQL_ENGINE = "2";
            ReturnObject result = new ReturnObject();
            SendMail mailer = new SendMail();
            string hostname = Environment.MachineName;
            int engine = new Int32();

            if (!File.Exists(@"E:\C#\Projects\sql_check\sql_check\bin\Debug\etc\settings.json"))
            {
                Environment.Exit(0);
            }

            Settings config = new Settings(@"E:\C#\Projects\sql_check\sql_check\bin\Debug\etc\settings.json");

            switch (args[0])
            {
                case ORACLE_ENGINE:
                    Console.WriteLine("Begining SQL Check from {0} --> {1}", hostname, config.server.oracle.host);
                    result = Functions.checkOracle(config.server);
                    engine = Convert.ToInt32(args[0]);
                    break;
                case MSSQL_ENGINE:
                    Console.WriteLine("Begining SQL Check from {0} --> {1}", hostname, config.server.mssql.instance);
                    result = Functions.checkMSSQL(config.server);
                    engine = Convert.ToInt32(args[0]);
                    break;
                default :
                    Console.WriteLine("Invalid Command line arguments");
                    Console.WriteLine(">sql_check.exe 1  -  To check ORACLE");
                    Console.WriteLine(">sql_check.exe 2  -  To check MSSQL");
                    Environment.Exit(10);
                    break;
            }

            foreach (string s in result.messages)
            {
                Console.WriteLine(s);
            }

            mailer.sendLogEmail(config, engine, result);

            Console.ReadLine();
        }


    }
}
